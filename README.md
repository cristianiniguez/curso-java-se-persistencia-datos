# Curso de Java SE Persistencia de Datos

Proyectos del [Curso de Java SE Persistencia de Datos](https://platzi.com/cursos/java-persistencia/) de Platzi

<p align="center">
  <img src="https://static.platzi.com/media/achievements/badge-java-se-persistencia-datos-ca571436-58b0-4303-91dd-2612cc3cddd0.png" alt="Badge del curso" width="100px" />
</p>

Mis apuntes: https://cristianiniguez.notion.site/Curso-de-Java-SE-Persistencia-de-Datos-c1aebdb63e9b410a82f67003abf0a2b4
