package com.mycompany.messages_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MessagesDAO {

    public static void createMessage(Message message) {
        try {
            Connection cn = DBConnection.getConnection();
            String query = "INSERT INTO `messages` (`content`, `author`) VALUES (?, ?);";

            PreparedStatement ps = cn.prepareStatement(query);
            ps.setString(1, message.getContent());
            ps.setString(2, message.getAuthor());
            ps.executeUpdate();
            System.out.println("Message created");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void readMessages() {
        try {
            Connection cn = DBConnection.getConnection();
            String query = "SELECT * FROM `messages`;";

            PreparedStatement ps = cn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                System.out.println("ID: " + rs.getInt("id"));
                System.out.println("Content: " + rs.getString("content"));
                System.out.println("Author: " + rs.getString("author"));
                System.out.println("Date: " + rs.getString("date"));
                System.out.println("");
            }

        } catch (SQLException e) {
            System.out.println("Messages could not be fetched");
            System.out.println(e);
        }
    }

    public static void deleteMessage(int id) {
        try {
            Connection cn = DBConnection.getConnection();
            String query = "DELETE FROM `messages` WHERE `id` = ?;";
            PreparedStatement ps = cn.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
            System.out.println("Message has been deleted");
        } catch (SQLException e) {
            System.out.println("Message could not be deleted");
            System.out.println(e);
        }
    }

    public static void updateMessage(Message message) {
        try {
            Connection cn = DBConnection.getConnection();
            String query = "UPDATE `messages` SET `content` = ? WHERE `id` = ?;";
            PreparedStatement ps = cn.prepareStatement(query);
            ps.setString(1, message.getContent());
            ps.setInt(2, message.getId());
            ps.executeUpdate();
            System.out.println("Message has been updated");
        } catch (SQLException e) {
            System.out.println("Message could not be updated");
            System.out.println(e);
        }
    }

}
