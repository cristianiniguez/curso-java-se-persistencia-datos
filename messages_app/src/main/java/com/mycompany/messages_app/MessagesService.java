package com.mycompany.messages_app;

import java.util.Scanner;

public class MessagesService {

    public static void createMessage() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Write your message");
        String content = sc.nextLine();

        System.out.println("What's your name?");
        String author = sc.nextLine();

        Message message = new Message();
        message.setContent(content);
        message.setAuthor(author);

        MessagesDAO.createMessage(message);
    }

    public static void readMessages() {
        MessagesDAO.readMessages();
    }

    public static void deleteMessage() {
        Scanner sc = new Scanner(System.in);

        System.out.println("What's the id of the message to delete?");
        int id = sc.nextInt();

        MessagesDAO.deleteMessage(id);
    }

    public static void updateMessage() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Write your new message");
        String content = sc.nextLine();

        System.out.print("What's the id of the message to update?");
        int id = sc.nextInt();

        Message message = new Message();
        message.setId(id);
        message.setContent(content);

        MessagesDAO.updateMessage(message);

    }

}
