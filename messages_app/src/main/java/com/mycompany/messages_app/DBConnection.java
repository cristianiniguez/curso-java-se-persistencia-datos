package com.mycompany.messages_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static Connection cn;
    private static final String URL = "jdbc:mysql://localhost:3306/platzi_messages_app";
    private static final String USER = "root";
    private static final String PASSWORD = "";

    public static Connection getConnection() throws SQLException {
        if (cn == null) {
            cn = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Connection successfull");
        }

        return cn;
    }
}
