package com.mycompany.messages_app;

import java.util.Scanner;

public class Index {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int option = 0;

        do {
            System.out.println("----------------------------------------");
            System.out.println("Messages Application");
            System.out.println("1. Create a message");
            System.out.println("2. Read messages");
            System.out.println("3. Update a message");
            System.out.println("4. Delete a message");
            System.out.println("5. Close");
            System.out.println("----------------------------------------");

            // Reading user option
            option = sc.nextInt();

            switch (option) {
                case 1 ->
                    MessagesService.createMessage();
                case 2 ->
                    MessagesService.readMessages();
                case 3 ->
                    MessagesService.updateMessage();
                case 4 ->
                    MessagesService.deleteMessage();
                case 5 -> {
                }
                default -> {
                }
            }
        } while (option != 5);
    }
}
