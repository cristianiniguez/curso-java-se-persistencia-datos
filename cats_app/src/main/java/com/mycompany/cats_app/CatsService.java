package com.mycompany.cats_app;

import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class CatsService {

    private static final String API_URL = "https://api.thecatapi.com/v1";
    private static final String API_KEY = "live_nkPbl2RxkzCRsCmNgkWzBlpnaygRdll9O4djUFkZE6LeAl3tVJ8n84SIf6pU2vi0";

    private static ImageIcon getCatImageIcon(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            Image image = ImageIO.read(url);
            ImageIcon catBg = new ImageIcon(image);

            if (catBg.getIconWidth() > 800) {
                Image background = catBg.getImage();
                Image modifiedBackground = background.getScaledInstance(800, 600, Image.SCALE_SMOOTH);
                catBg = new ImageIcon(modifiedBackground);
            }

            return catBg;
        } catch (IOException e) {
            System.out.println(e);
            return null;
        }
    }

    public static void watchCat() throws IOException {

        // calling the api
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(API_URL + "/images/search")
                .get()
                .build();
        Response response = client.newCall(request).execute();

        // parsing the body, removing surrounding brackets []
        String body = response.body().string();
        body = body.substring(1, body.length());
        body = body.substring(0, body.length() - 1);

        // converting data to object
        Gson gson = new Gson();
        Cat cat = gson.fromJson(body, Cat.class);
        ImageIcon catBg = getCatImageIcon(cat.getUrl());

        final String menu = """
                            Options:
                            1. Watch another image
                            2. Mark as favorite
                            3. Return to Menu
                            """;

        final String WATCH_ANOTHER_IMAGE_OPTION = "Watch another image";
        final String MARK_AS_FAVORITE_OPTION = "Mark as favorite";
        final String RETURN_TO_MENU_OPTION = "Return to Menu";
        String[] buttons = {WATCH_ANOTHER_IMAGE_OPTION, MARK_AS_FAVORITE_OPTION, RETURN_TO_MENU_OPTION};

        String option = (String) JOptionPane.showInputDialog(
                null,
                menu,
                cat.getId(),
                JOptionPane.INFORMATION_MESSAGE,
                catBg,
                buttons,
                buttons[0]
        );

        switch (option) {
            case WATCH_ANOTHER_IMAGE_OPTION ->
                watchCat();
            case MARK_AS_FAVORITE_OPTION ->
                favCat(cat);
            default -> {
                break;
            }

        }
    }

    public static void favCat(Cat cat) {
        try {
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\"image_id\":\"" + cat.getId() + "\"}");
            Request request = new Request.Builder()
                    .url(API_URL + "/favourites")
                    .method("POST", body)
                    .addHeader("x-api-key", API_KEY)
                    .addHeader("Content-Type", "application/json")
                    .build();
            client.newCall(request).execute();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void watchFavCats() throws IOException {
        // calling the api
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(API_URL + "/favourites")
                .get()
                .addHeader("x-api-key", API_KEY)
                .build();
        Response response = client.newCall(request).execute();
        String body = response.body().string();

        Gson gson = new Gson();
        FavCat[] favCats = gson.fromJson(body, FavCat[].class);

        if (favCats.length > 0) {
            FavCat favCat = favCats[(int) Math.floor(Math.random() * favCats.length)];
            ImageIcon favCatBg = getCatImageIcon(favCat.getImage().getUrl());

            final String menu = """
                            Options:
                            1. Watch favorites
                            2. Unmark as favorite
                            3. Return to Menu
                            """;

            final String WATCH_FAVORITES_OPTION = "Watch favorites";
            final String UNMARK_AS_FAVORITE_OPTION = "Unmark as favorite";
            final String RETURN_TO_MENU_OPTION = "Return to Menu";
            String[] buttons = {WATCH_FAVORITES_OPTION, UNMARK_AS_FAVORITE_OPTION, RETURN_TO_MENU_OPTION};

            String option = (String) JOptionPane.showInputDialog(
                    null,
                    menu,
                    favCat.getId(),
                    JOptionPane.INFORMATION_MESSAGE,
                    favCatBg,
                    buttons,
                    buttons[0]
            );

            switch (option) {
                case WATCH_FAVORITES_OPTION ->
                    watchFavCats();
                case UNMARK_AS_FAVORITE_OPTION ->
                    unfavCat(favCat);
                default -> {
                    break;
                }

            }
        }
    }

    private static void unfavCat(FavCat favCat) {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(API_URL + "/favourites/" + favCat.getId())
                    .delete()
                    .addHeader("x-api-key", API_KEY)
                    .build();
            client.newCall(request).execute();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
