package com.mycompany.cats_app;

public class FavCat {

    private String id;
    private String image_id;
    private FavCatImage image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public FavCatImage getImage() {
        return image;
    }

    public void setImage(FavCatImage image) {
        this.image = image;
    }

}
