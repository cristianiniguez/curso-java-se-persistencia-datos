package com.mycompany.cats_app;

import java.io.IOException;
import javax.swing.JOptionPane;

public class Index {

    public static void main(String[] args) throws IOException {
        int menuOption = -1;

        String[] buttons = {
            "1. Watch cats",
            "2. Watch favourites",
            "3. Close"
        };

        do {
            String option = (String) JOptionPane.showInputDialog(
                    null,
                    "Java Cats",
                    "Main menu",
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    buttons,
                    buttons[0]
            );

            for (int i = 0; i < buttons.length; i++) {
                if (option.equals(buttons[i])) {
                    menuOption = i;
                }
            }

            switch (menuOption) {
                case 0 -> CatsService.watchCat();
                case 1 -> CatsService.watchFavCats();
                default -> {
                }
            }

        } while (menuOption != 2);
    }
}
